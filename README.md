# Check MegaRAID Health

These scripts can be used to check health of disks in MegaRAID array using S.M.A.R.T database and RAID controller, and report via Mail, Nagios or Icinga.
